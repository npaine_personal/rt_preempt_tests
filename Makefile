CC = gcc
CFLAGS = -Wall

all: test_rt kern_detect

test_rt:
	$(CC) -o test_rt test_rt.c -lrt

kern_detect:
	$(CC) -o kern_detect kern_detect.c -lrt

clean: 
	rm test_rt kern_detect